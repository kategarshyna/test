(function($) {
    var state = $('#state');
    var error = $('#error');
    var currenttime = $('#currenttime');
    var duration = $('#duration');
    var currenttimetext = $('#currenttime-txt');
    var volume_selector =  $('#volume');
    var volume_label = $('#volume-label');
    var volume_text = $('#volume-txt');
    var play = $('#play');
    var pause = $('#pause');
    var play_main = $('#main-play');
    var source = $('video#video source');
    var video = $('video#video');
    var tag = 'video';
    var sliderEl1 = document.querySelector('#volume[data-rangeslider]');


    rangesliderJs.create(sliderEl1);

    /**
     * Player JS
     */
    function initEmbed() {
        $(this).on('embedplayer:statechange', function (event) {
            state.text(event.state);
        }).on('embedplayer:error', function (event) {
            var message = event.error||'';
            if (event.title)        { message += " "+event.title; }
            else if (event.message) { message += " "+event.message; }
            error.text(message);
        }).on('embedplayer:durationchange', function (event) {
            if (isFinite(event.duration)) {
                currenttime.show().prop('max', event.duration);
            }
            else {
                currenttime.hide();
            }
            duration.text(event.duration.toFixed(0).toHHMMSS());
        }).on('embedplayer:timeupdate', function (event) {
            currenttime.val(event.currentTime);
            currenttimetext.text(event.currentTime.toFixed(0).toHHMMSS());
        }).on('embedplayer:volumechange', function (event) {
            volume_selector.val(event.volume);
            volume_label.html(
                event.volume <=   0 ? '<img src="/images/mute.png"/>' :
                    event.volume <= 1/3 ? '<img src="/images/sound-small.png"/>' :
                        event.volume <= 2/3 ? '<img src="/images/sound-middle.png"/>' :
                            '<img src="/images/sound.png"/>'
            );
            volume_text.text(event.volume.toFixed(2));
        }).on('embedplayer:play', function (event) {
                play.removeClass('hidden').addClass('hidden');
                play_main.removeClass('hidden').addClass('hidden');
                pause.removeClass('hidden');
        }).on('embedplayer:pause', function (event) {
            pause.removeClass('hidden').addClass('hidden');
            play.removeClass('hidden');
            play_main.removeClass('hidden');
        }).
        embedplayer("listen").
        embedplayer('volume', function (volume) {
            volume_selector.text(volume.toFixed(2));
        });
    }
    String.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10);
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        if (hours === '00') {
            return minutes+':'+seconds;
        }
        return hours+':'+minutes+':'+seconds;
    };

    $.each(video, initEmbed);

    play.on('click', function() { video.embedplayer('play') });
    play_main.on('click', function() { video.embedplayer('play') });
    pause.on('click', function() { video.embedplayer('pause') });
    volume_selector.on('input', function() { video.embedplayer('volume', +this.value) });
    /**
     * END Player
     */

})(jQuery);
